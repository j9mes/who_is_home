import subprocess
import requests
import collections
from local_settings import *
from time import sleep, strftime
from threading import Thread

# If running at startup, sleep once right when this script is called to give the Pi enough time
# to connect to the network
# sleep(60)

# Some arrays to help minimize posting and account for devices
# disappearing from the network when asleep
firstRun = [1] * len(occupant)
presentSent = [0] * len(occupant)
notPresentSent = [0] * len(occupant)
counter = [0] * len(occupant)


# Function that checks for device presence
def who_is_here(i):
    # 30 second pause to allow main thread to finish arp-scan and populate output
    sleep(30)
    # Loop through checking for devices and counting if they're not present
    while True:

        # Exits thread if Keyboard Interrupt occurs
        if stop:
            print("Exiting Thread")
            exit()
        else:
            pass

        # If a listed device address is present print and post
        if address[i] in output:
            print(f"{strftime('%H:%M:%S')} - {occupant[i]}'s device is connected to the network ")
            if presentSent[i] == 0:

                # device is present but "is home" notification has not yet been posted:

                # POST "arrived home" notification HERE
                channel_msg(chan, f'{occupant[i]} arrived home')

                # DO ANYTHING ELSE HERE

                # Reset counters
                firstRun[i] = 0
                presentSent[i] = 1
                notPresentSent[i] = 0
                counter[i] = 0
                sleep(900)  # wait for 15 minutes
            else:
                # If "is home" notification has already been posted, wait for 15 minutes
                counter[i] = 0
                sleep(900)  # wait for 15 minutes
        # If a listed device address is not present, print and DO SOMETHING
        else:
            print(f"{strftime('%H:%M:%S')} - {occupant[i]}'s device is NOT connected to the network ")
            # Only consider a device offline if it's counter has reached 30
            # This is the same as 15 minutes passing
            if counter[i] == 30 or firstRun[i] == 1:
                firstRun[i] = 0
                if notPresentSent[i] == 0:

                    # device is not present, but "has left home" notification has not yet been posted:

                    # POST "has left home" notification HERE
                    channel_msg(chan, f'{occupant[i]} has left home')

                    # DO ANYTHING ELSE HERE

                    # Reset counters
                    notPresentSent[i] = 1
                    presentSent[i] = 0
                    counter[i] = 0
                else:
                    # If "has left home" notification has already been posted, wait for 30 seconds
                    counter[i] = 0
                    sleep(30)

            # Count how many 30 second intervals have happened since the device 
            # disappeared from the network
            else:
                counter[i] = counter[i] + 1
                print(occupant[i] + "'s counter at " + str(counter[i]))
                sleep(30)


SlackChannel = collections.namedtuple('SlackChannel', 'name channel token webhook')

chan = SlackChannel(name=name, channel=channel, token=token, webhook=webhook)


def channel_msg(slack_channel, text):
    """
    post a text message to a slack channel
    :param slack_channel: str
    :param text: str (message)

    """
    requests.post(slack_channel.webhook, json={'text': text})


# Main thread

try:

    # Initialize a variable to trigger threads to exit when True

    stop = False

    # Start the thread(s)
    # It will start as many threads as there are values in the occupant array
    for i in range(len(occupant)):
        t = Thread(target=who_is_here, args=(i,))
        t.start()

    while True:
        # Assign list of devices on the network to "output"
        output = str(subprocess.check_output("arp-scan --localnet", shell=True))
        # Wait 20 seconds between scans
        sleep(20)

except KeyboardInterrupt:
    # On a keyboard interrupt signal threads to exit
    stop = True
    exit()
